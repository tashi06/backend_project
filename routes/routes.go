package routes

import (
	"GO/controller"
	"net/http"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()

	// USER ROUTE
	router.HandleFunc("/user/signup", controller.UserRegistration).Methods("POST")
	router.HandleFunc("/user/login", controller.UserLogin).Methods("POST")
	router.HandleFunc("/user/forgotPassword/{email}", controller.UserForgotPassword).Methods("GET")
	router.HandleFunc("/user/passwordResetForm", controller.UserPasswordReseter).Methods("POST")
	router.HandleFunc("/logout", controller.Logout)

	// RECIPE ROUTE
	router.HandleFunc("/recipe/add", controller.AddRecipe).Methods("POST")
	router.HandleFunc("/recipe/{rid}", controller.GetRecipe).Methods("GET")
	router.HandleFunc("/recipes", controller.GetAllRecipe).Methods("GET")
	router.HandleFunc("/recipe/delete/{rid}", controller.DeleteRecipe).Methods("DELETE")
	router.HandleFunc("/recipe/update/{rid}", controller.UpdateRecipe).Methods("PUT")

	fileHandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fileHandler)

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		return
	}
}
