package controller

import (
	"GO/model"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"GO/utils/httpResp"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

func UserRegistration(w http.ResponseWriter, r *http.Request) {

	// User instance
	var user model.User

	// Request Handler
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}

	defer r.Body.Close()

	plainPassword := user.Password

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(plainPassword), bcrypt.DefaultCost)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Passwrd Hashing Error")
		return
	}

	user.Password = string(hashedPassword)

	// Model Handler
	registrationErr := user.Registration()

	// Response Handler
	if registrationErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, registrationErr.Error())
	} else {
		httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "User Added"})
	}
}

func UserLogin(w http.ResponseWriter, r *http.Request) {
	// User instance
	var user model.User

	// Request Handler
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json body")
		return
	}
	defer r.Body.Close()

	plainPassword := user.Password

	credentialErr := user.CheckCredential()

	if credentialErr != nil {
		switch credentialErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Invalid Credential")
			return
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, credentialErr.Error())
			return
		}
	}

	hashedPassword := user.Password

	passwordErr := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(plainPassword))

	// if passwordErr == bcrypt.ErrMismatchedHashAndPassword {
	// 	fmt.Println("Invalid")
	// }

	if passwordErr != nil {
		httpResp.RespondWithError(w, http.StatusNotFound, "Invalid Credential")
		return
	} else {
		cookie := http.Cookie{
			Name:     "CooksCompass",
			Value:    user.Email,
			Expires:  time.Now().Add(30 * time.Minute),
			Secure:   true,
			HttpOnly: true,
			Path:     "/",
		}

		http.SetCookie(w, &cookie)
		httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "Success"})
	}
}

func UserForgotPassword(w http.ResponseWriter, r *http.Request) {

	userEmail := mux.Vars(r)["email"]
	user := model.User{Email: userEmail}

	forgotPasswordCredential := user.UserValidation()

	if forgotPasswordCredential != nil {
		switch forgotPasswordCredential {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Invalid Credential")
			return
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, forgotPasswordCredential.Error())
			return
		}
	}

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "Valid Credential"})
}

func UserPasswordReseter(w http.ResponseWriter, r *http.Request) {

	var user model.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json body")
		return
	}

	plainPassword := user.Password

	fmt.Println(plainPassword)

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(plainPassword), bcrypt.DefaultCost)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Passwrd Hashing Error")
		return
	}

	user.Password = string(hashedPassword)

	fmt.Println(user.Password)

	passwordErr := user.UpdatePassword()

	if passwordErr != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, passwordErr.Error())
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "Password Changed"})
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "CooksCompass",
		Expires: time.Now(),
	})

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "cookie deleted"})
}

func VerifyCookie(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("CooksCompass")
		if err != nil {
			if err == http.ErrNoCookie {
				// Return an error to be handled by an error handler or the next middleware
				httpResp.RespondWithError(w, http.StatusSeeOther, "Cookie not found")
				return
			}
			// Return an error to be handled by an error handler or the next middleware
			httpResp.RespondWithError(w, http.StatusInternalServerError, "Inter server error")
			return
		}

		if cookie.Value != "CooksCompass" {
			// Return an error to be handled by an error handler or the next middleware
			httpResp.RespondWithError(w, http.StatusSeeOther, "Cookie does not match")
			return
		}

		// Call the next handler in the chain
		next.ServeHTTP(w, r)
	})
}
