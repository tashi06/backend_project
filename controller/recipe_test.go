package controller_test

import (
	"GO/controller"
	"GO/model"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
)

func TestGetAllRecipe(t *testing.T) {
	// Create a new request
	req, err := http.NewRequest("GET", "/recipes", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Add a cookie to the request
	req.AddCookie(&http.Cookie{Name: "CooksCompass", Value: "examplecookie"})

	// Create a response recorder to record the response
	rr := httptest.NewRecorder()

	// Call the GetAllRecipe handler function
	handler := http.HandlerFunc(controller.GetAllRecipe)
	handler.ServeHTTP(rr, req)

	// Check the response status code
	if rr.Code != http.StatusOK {
		t.Errorf("GetAllRecipe returned wrong status code: got %v, want %v", rr.Code, http.StatusOK)
	}

	// Parse the response body
	var recipes []model.Recipe
	err = json.Unmarshal(rr.Body.Bytes(), &recipes)
	if err != nil {
		t.Fatal(err)
	}

	// Check the expected values in the response
	// ...
}

func TestGetRecipe_NotFound(t *testing.T) {
	// Create a new request with a route parameter
	req, err := http.NewRequest("GET", "/recipe/{rid}", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Set the route parameter
	req = mux.SetURLVars(req, map[string]string{"rid": "999"})

	// Create a response recorder to record the response
	rr := httptest.NewRecorder()

	// Call the GetRecipe handler function
	handler := http.HandlerFunc(controller.GetRecipe)
	handler.ServeHTTP(rr, req)

	// Check the response status code
	if rr.Code != http.StatusNotFound {
		t.Errorf("GetRecipe returned wrong status code: got %v, want %v", rr.Code, http.StatusNotFound)
	}

	// Check the error message in the response
	expectedError := "Recipe not found"
	if !strings.Contains(rr.Body.String(), expectedError) {
		t.Errorf("GetRecipe returned unexpected error: got %v, want %v", rr.Body.String(), expectedError)
	}
}

// You can add more test cases for edge cases and error conditions.

func TestMain(m *testing.M) {
	// Set up any test dependencies or configurations

	// Run the tests
	m.Run()
}
