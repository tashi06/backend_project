package model

import "GO/database/postgres"

type User struct {
	FirstName string `json:"fname"`
	LastName  string `json:"lname"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

const (
	queryInsertUser     = "INSERT INTO users (FirstName,LastName,Email,UserPassword) VALUES ($1,$2,$3,$4) RETURNING Email;"
	queryFindUser       = "SELECT UserPassword FROM users WHERE Email=$1;"
	queryUserValidation = "SELECT Email FROM users WHERE Email=$1;"
	queryPassword       = "UPDATE users SET UserPassword=$1 WHERE Email=$2 RETURNING Email;"
)

func (u *User) Registration() error {
	return postgres.Db.QueryRow(queryInsertUser, u.FirstName, u.LastName, u.Email, u.Password).Scan(&u.Email)
}

func (u *User) CheckCredential() error {
	return postgres.Db.QueryRow(queryFindUser, u.Email).Scan(&u.Password)
}

func (u *User) UserValidation() error {
	return postgres.Db.QueryRow(queryUserValidation, u.Email).Scan(&u.Email)
}

func (u *User) UpdatePassword() error {
	return postgres.Db.QueryRow(queryPassword, u.Password, u.Email).Scan(&u.Email)
}
