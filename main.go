package main

import (
	"GO/routes"
)

func main() {
	routes.InitializeRoutes()
}
