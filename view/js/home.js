window.onload = () => {
    fetch("/recipes")
    .then(res => res.text())
    .then(data => dataConverter(data))
    .catch(e => console.log(e))
}

function dataConverter(dataCollection) {
    let data = JSON.parse(dataCollection)
    generateCard(data)
}

function generateCard(data){
    // Generate the cards dynamically
    var cardGrid = document.getElementById("cardGrid");
    var columnCount = 3;
    var rowCount = Math.ceil(data.length / columnCount);

    for (var row = 0; row < rowCount; row++) {
        for (var col = 0; col < columnCount; col++) {
            var index = row * columnCount + col; 
            if (index >= data.length) {
                break;
            }
            
            var cardData = data[index];

            var card = document.createElement("div");
            card.className = "card";
    
            var image = document.createElement("img");
            image.src = cardData.image;
            image.alt = cardData.title;
    
            var title = document.createElement("h3");
            title.textContent = cardData.recipeName;
    
            var description = document.createElement("p");
            var truncatedDescription = truncateText(cardData.description, 16);
            description.textContent = truncatedDescription;
    
            var button = document.createElement("button");
            button.textContent = "View More";
            button.addEventListener("click", function(data) {
                return function() {
                    var url = "recipe.html" +
                    "?title=" + encodeURIComponent(data.recipeName) +
                    "&description=" + encodeURIComponent(data.description) +
                    "&image=" + encodeURIComponent(data.image) +
                    "&ingredients=" + encodeURIComponent(data.ingredient) +
                    "&instructions=" + encodeURIComponent(data.instruction) +
                    "&recipeId="+encodeURIComponent(cardData.RecipeID);
                    window.location.href = url;
                };
            }(cardData));
    
            card.appendChild(image);
            card.appendChild(title);
            card.appendChild(description);
            card.appendChild(button);
    
            cardGrid.appendChild(card);
        }
    }
}

// Function to truncate text to a specified number of words
function truncateText(text, wordLimit) {
    var words = text.split(" ");
    if (words.length > wordLimit) {
        var truncatedWords = words.slice(0, wordLimit);
        return truncatedWords.join(" ") + " ...";
    }
    return text;
}

