function getCookieValue(cookieName) {
    const cookieString = document.cookie;
    const cookieArray = cookieString.split(';');
    
    for (let i = 0; i < cookieArray.length; i++) {
      const cookie = cookieArray[i].trim();
      if (cookie.startsWith(cookieName + '=')) {
        return cookie.substring(cookieName.length + 1);
      }
    }
    
    return null; // Cookie not found
  }
  
  // Example usage:
  const myCookieValue = getCookieValue('CooksCompass');
  console.log(myCookieValue);