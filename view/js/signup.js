var emailInput = document.getElementById('fname')
var emailInput = document.getElementById('email')
var passwordInput = document.getElementById('password')
var confirmPasswordInput = document.getElementById('confirm_password')
var form = document.getElementById("signUp-form")

const validateForm = () => {
    let  isFnameValid = checkFname(),
         isEmailValid = checkEmail(),
         isPasswordValid = checkPassword(),
         isConfirmPasswordValid = checkConfrimPassword();
     let isFormValid = isFnameValid && isEmailValid && isPasswordValid && isConfirmPasswordValid;

     if (isFormValid){

        const data = {
            fname:fname.value,
            lname:document.getElementById('name').value,
            email:emailInput.value,
            password:passwordInput.value,
        }

        fetch("/user/signup",{
            method:"POST",
            body:JSON.stringify(data),
            headers: {"Content-type":"application/json; charset=UTF-8"}
        }).then((res) => {
            if (res.status === 201){
                alert("Account Created")
                window.location.href = "login.html";
            }else{
                throw new Error(res.statusText)
            }
        }).catch((e) => {
            console.log(e);
        })
          
     }
}

var fnameInput = document.getElementById('fname');
var emailInput = document.getElementById('email');
var passwordInput = document.getElementById('password');
var confirmPasswordInput = document.getElementById('confirm_password');

var fnameError = document.getElementById('fnameError');
var emailError = document.getElementById('emailError');
var passwordError = document.getElementById('passwordError');
var confirmPasswordError = document.getElementById('confirm_passwordError');

fnameInput.addEventListener('input', function(){
    checkFname()
})
emailInput.addEventListener('input', function(){
    checkEmail()
})
passwordInput.addEventListener('input',function(){
    checkPassword();
})
confirmPasswordInput.addEventListener('input', function(){
    checkConfrimPassword();
})

const checkFname = () => {
    if (!isRequired(fnameInput.value)){
        fnameInput.classList.add('is-invalid');
        fnameInput.classList.remove('is-valid');
        fnameError.innerHTML = 'First name cannot be blank.';
        return false;
    } else{
        fnameInput.classList.add('is-valid');
        fnameInput.classList.remove('is-invalid');
        fnameError.innerHTML = '';
        return true;
    }
}


const checkEmail = () => {
    if (!isRequired(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email cannot be blank.';
        return false;
    } else if (!isEmailValid(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email is not valid.';
        return false;
    } else{
        emailInput.classList.add('is-valid');
        emailInput.classList.remove('is-invalid');
        emailError.innerHTML = '';
        return true;
    }
}
const isEmailValid = (email) => {
    const re = /^[A-Za-z0-9._%+-]+@[a-z]+\.com$/;
    // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
}

const isRequired = value => value.trim() === ''? false : true;
// const isRequired = value => !value;

const checkPassword = () => {
    if (!isRequired(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password cannot be blank.';
        return false;
    } else if(!isPasswordValid(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password must has at least 8 characters that include atleast 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character';
        return false;
    } else {
        passwordInput.classList.add('is-valid');
        passwordInput.classList.remove('is-invalid');
        passwordError.innerHTML = '';
        return true;
    }
}
const isPasswordValid = (password) => {
    const re = new
    RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    return re.test(password);
}

checkConfrimPassword = () => {
    if (!isRequired(confirmPasswordInput.value)){
        confirmPasswordInput.classList.add('is-invalid');
        confirmPasswordInput.classList.remove('is-valid');
        confirmPasswordError.innerHTML = 'Please enter the password again.';
        return false;
    } else if (confirmPasswordInput.value !== passwordInput.value){
        confirmPasswordInput.classList.add('is-invalid');
        confirmPasswordInput.classList.remove('is-valid');
        confirmPasswordError.innerHTML = 'The password does not match.';
        return false;
    } else {
        confirmPasswordInput.classList.add('is-valid');
        confirmPasswordInput.classList.remove('is-invalid');
        confirmPasswordError.innerHTML = '';
        return true;
    }
}