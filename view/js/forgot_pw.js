var emailInput = document.getElementById('email');

    const validateForm = () => {
        let isEmailValid = checkEmail();
        let isFormValid = isEmailValid;

        if (isFormValid) {
            fetch(`/user/forgotPassword/${emailInput.value}`,{
                method:"GET",
                headers: {"Content-type":"application/json; charset=UTF-8"}
            }).then((res) => {
                if (res.ok){
                    sessionStorage.setItem("Email",emailInput.value)
                    window.location.href = "rewrite_pw.html";
                }else if(res.status === 404) {
                    alert("Invalid Credential");
                }else{
                    throw new Error(res.statusText)
                }
            }).catch((e) => {
                console.log(e);
            })
            
        }
    }


    var emailInput = document.getElementById('email');
    var emailError = document.getElementById('emailError');

    emailInput.addEventListener('input', function(){
        checkEmail()
    })

    const checkEmail = () => {
        const email = emailInput.value.trim()
        if (!isRequired(emailInput.value)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email cannot be blank.';
            return false;
        } else if (!isEmailValid(email)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email is not valid.';
            return false;
        } else{
            emailInput.classList.add('is-valid');
            emailInput.classList.remove('is-invalid');
            emailError.innerHTML = '';
            return true;
        }
    }
    
    const isRequired = value => value.trim() === ''? false : true;

    const isEmailValid = (email) => {
        const re = /^[A-Za-z0-9._%+-]+@[a-z]+\.com$/;
        return re.test(email)
    }
