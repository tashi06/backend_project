const urlParams = new URLSearchParams(window.location.search);
const title = urlParams.get('title');
const description = urlParams.get('description');
const image = urlParams.get('image');
const ingredients = urlParams.get('ingredients');
const instructions = urlParams.get('instructions');
const recipeId =  urlParams.get('recipeId')
    
// Set the values in the recipe details page
document.getElementById('title').textContent = title;
document.getElementById('description').textContent = description;
document.querySelector('.column1 img').src = image;
    
// Split the ingredients string into an array and display them
const ingredientsList = ingredients.split('\n');
const ingredientsListElement = document.getElementById('ingredients');

ingredientsList.forEach(function(ingredient) {
    const li = document.createElement('li');
    li.textContent = ingredient;
    ingredientsListElement.appendChild(li);
});
    
// Split the instructions string into an array and display them
const instructionsList = instructions.split('\n');
const instructionsListElement = document.getElementById('instructions');
instructionsList.forEach(function(instruction) {
    const li = document.createElement('li');
    li.textContent = instruction;
    instructionsListElement.appendChild(li);
});
    
// Add event listeners to the edit and delete buttons
document.getElementById('editButton').addEventListener('click', function() {
    window.location.href = "updateRecipe.html";
});
document.getElementById('deleteButton').addEventListener('click', function() {
    var confirmation = confirm("Are you sure you want to delete this recipe?");
    if (confirmation) {
        fetch(`/recipe/delete/${recipeId}`,{method:"DELETE"})
        .then(res => {
            if (res.ok){
                alert("Recipe Deleted")
                window.location.href = "home.html";
            }
        })
        .catch(e => console.log(e))
        
    }
});
          