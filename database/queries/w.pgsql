CREATE TABLE users (
        UserID SERIAL PRIMARY KEY,
        FirstName varchar(100) NOT NULL,
        LastName varchar (100) DEFAULT NULL,
        Email varchar(100) NOT NULL,
        UserPassword varchar(100) NOT NULL,
        UNIQUE(Email)
);

CREATE TABLE recipes (
        RecipeID SERIAL PRIMARY KEY,
        UserEmail varchar(100) REFERENCES users(Email) ON DELETE CASCADE ON UPDATE CASCADE,
        RecipeName varchar(60) NOT NULL,
        Description TEXT NOT NULL,
        Ingredient varchar(255) NOT NULL,
        Instruction varchar(255) NOT NULL,
        Image TEXT NOT NULL
);